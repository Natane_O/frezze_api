Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get  'users'    , to: 'users#index'  , as: 'users'
  get  'users/:id', to: 'users#show'   , as: 'user' , id:/\d+/
  get  'users/new', to: 'users#new'    , as: 'new_user'
  post 'users'    , to: 'users#create'


  get  'products'    , to: 'products#index'  , as: 'products'
  get  'products/:id', to: 'products#show'   , as: 'product' , id:/\d+/
  get  'products/new', to: 'products#new'    , as: 'new_product'
  post 'products'    , to: 'products#create'


  get  'orders'    , to: 'orders#index'  , as: 'orders'
  get  'orders/:id', to: 'orders#show'   , as: 'order' , id:/\d+/
  get  'orders/new', to: 'orders#new'    , as: 'new_order'
  post 'orders'    , to: 'orders#create'

  get  'product_orders'    , to: 'product_orders#index'  , as: 'product_orders'
  get  'product_orders/:id', to: 'product_orders#show'   , as: 'product_order' , id:/\d+/
  get  'product_orders/new', to: 'product_orders#new'    , as: 'new_product_order'
  post 'product_orders'    , to: 'product_orders#create'
end
