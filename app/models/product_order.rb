class ProductOrder < ApplicationRecord

  belongs_to :order

  def product_first

    Product.find(first_product)
    
  end

  def product_second

    Product.find(second_product)
    
  end
  
end
