class ProductOrdersController < ApplicationController
    
     def index
       @product_orders = ProductOrder.all
   
       render json: @product_orders
     end
   
     # GET /products/1
     def show
       @product_order = ProductOrder.find(params[:id])
       render json: @product_order
     end
   
     def new 
       @product_order = ProductOrder.new
     end

     def create
       @product_order = ProductOrder.new(product_order_params)
   
       if @product_order.save
         render json: @product_order, status: :created, location: @product_order
       else
         render json: @product_order.errors, status: :unprocessable_entity
       end
     end

       # Only allow a trusted parameter "white list" through.
       def product_order_params
         params.require(:product_order).permit(:flavor, :description, :weight, :value_cal, :vegan, :gluten_free, :organic)
       end
   end