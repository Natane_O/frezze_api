class UsersController < ApplicationController
    
     def index
       @users = User.all
   
       render json: @users
     end
   
     # GET /Users/1
     def show
       @user = User.find(params[:id])
       render json: @user
     end
   
     def new 
       @user = User.new
     end

     def create
       @user = User.new(user_params)
   
       if @user.save
         render json: @user, status: :created, location: @user
       else
         render json: @user.errors, status: :unprocessable_entity
       end
     end
  
       # Only allow a trusted parameter "white list" through.
       def user_params
         params.require(:user).permit(:name, :cpf, :email, :phone, :client_address, :type)
       end
   end