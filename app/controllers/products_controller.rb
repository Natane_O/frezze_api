class ProductsController < ApplicationController
    
     def index
       @products = Product.all
   
       render json: @products
     end
   
     # GET /products/1
     def show
       @product = Product.find(params[:id])
       render json: @product
     end
   
     def new 
       @product = Product.new
     end

     def create
       @product = Product.new(product_params)
   
       if @product.save
         render json: @product, status: :created, location: @product
       else
         render json: @product.errors, status: :unprocessable_entity
       end
     end

       # Only allow a trusted parameter "white list" through.
       def product_params
         params.require(:product).permit(:flavor, :description, :weight, :value_cal, :vegan, :gluten_free, :organic)
       end
   end