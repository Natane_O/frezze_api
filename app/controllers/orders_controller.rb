class OrdersController < ApplicationController
    
     def index
       @orders = Order.all
   
       render json: @orders
     end
   
     # GET /orders/1
     def show
       @order = Order.find(params[:id])
       render json: @order
     end
   
     def new 
       @order = Order.new
     end

     def create
       @order = Order.new(order_params)
   
       if @order.save
         render json: @order, status: :created, location: @order
       else
         render json: @order.errors, status: :unprocessable_entity
       end
     end

       # Only allow a trusted parameter "white list" through.
       def order_params
         params.require(:order).permit(:client, :delivery_time, :status, :comments, :delivery_address)
       end
   end