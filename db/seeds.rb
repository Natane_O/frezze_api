# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

  Product.create({flavor:"Calabresa", description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat", weight:"300gr", value_cal:"10/gr", vegan: true, gluten_free: true, organic: false, img:"calabresa" })
  Product.create({flavor:"Marguerita", description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat", weight:"300gr", value_cal:"10/gr", vegan: true, gluten_free: true, organic: false, img:"marguerita" })
  Product.create({flavor:"mussarela", description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat", weight:"300gr", value_cal:"10/gr", vegan: true, gluten_free: true, organic: false, img:"mussarela" })
  Product.create({flavor:"quatro queijos", description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat", weight:"300gr", value_cal:"10/gr", vegan: true, gluten_free: true, organic: false, img:"quatroqueijos" })
  Product.create({flavor:"portuguesa", description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat", weight:"300gr", value_cal:"10/gr", vegan: true, gluten_free: true, organic: false, img:"portuguesa" })
  Product.create({flavor:"parisiense", description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat", weight:"300gr", value_cal:"10/gr", vegan: true, gluten_free: true, organic: false, img:"parisiense" })

