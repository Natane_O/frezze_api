class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string  :flavor
      t.text    :description
      t.float   :weight
      t.float   :value_cal
      t.boolean :vegan
      t.boolean :gluten_free
      t.boolean :organic
      t.string  :img
      t.timestamps
    end
  end
end
