class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :user_type
      t.string :cpf
      t.string :email
      t.string :phone
      t.text   :client_address
      t.timestamps
    end
  end
end
