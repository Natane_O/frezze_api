class CreateProductOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :product_orders do |t|
      t.references :order, null: false, foreign_key: true
      t.integer    :first_product
      t.integer    :second_product
      t.boolean    :half_n_half
      t.string     :thickness
      t.boolean    :extra_sauce
      t.boolean    :vegan
      t.boolean    :gluten_free
      t.boolean    :organic  


      t.timestamps
    end
  end
end
