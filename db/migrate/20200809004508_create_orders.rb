class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table   :orders do |t|
      t.references :user, null: false, foreign_key: true
      t.datetime   :delivery_time
      t.string     :status
      t.text       :comments
      t.text       :delivery_address
      
      t.timestamps
    end
  end
end
